let compositionList = [{
    name: 'Тима Белорусских "Витаминка"',
    path: "../audios/hiti_2019_-_tima_belorusskih_-_vitaminka.mp3",
},
    {
        name: 'Дискотека Авария "Новый год"',
        path: "../audios/novogodnie_pesni_2016_-_diskoteka_avarija_-_novij_god_k_nam_mchitsja_(zv.fm).mp3",
    }];
let currentPlayer = 0;
let playBtn, audioContainer, addPlayerBtn, volume, player, progressBar, myBar;
addPlayer = () => {
    addPlayerBtn = document.getElementById('addPlayer');
    if (compositionList.length > 0 && currentPlayer < compositionList.length) {
        addPlayerContainer(currentPlayer);
        currentPlayer++;
        console.log(`this is number of players: ${currentPlayer}`);
        if (currentPlayer === compositionList.length) {
            let a = document.getElementById('addPlayer');
            console.log(`this is a: ${a}`);
            console.log(`this is addBtn: ${addPlayerBtn}`);
            addPlayerBtn.innerHTML = 'All players are on the form';

        }
    } else if (compositionList.length === 0) {
        addPlayerBtn.innerHTML = 'Composition list is empty';
    }
};
addPlayerContainer = (index) => {
    audioContainer = document.createElement('div');
    audioContainer.classList.add('playerContainer');
    audioContainer.innerHTML = `<audio id="audioPlayer-${index}">` +
        `<source src="${compositionList[index].path}">` +
        `</audio>` +
        `<p>${compositionList[index].name}</p>` +
        `<div class="column">` +
        `<input type="image" alt="" src="../img/play-button.png" onclick="handleAudio(${index})" class="audioBtn pause img"` +
        `id="playBtn-${index}">` +
        `<span id="current-time-${index}">0:00</span>` +
        `</div>` +
        `<div id="myProgress-${index}" class="myProgress column">
             <div id="myBar-${index}" class="myBar"></div>
        </div>` +
        `<div class="column">` +
        `        <span id="left-time-${index}"></span>` +
        `        <img alt="" src="../img/icon.png" id="volImg-${index}" onclick="showVolumeRange(${index})" class="img">` +
        `        <input type="range" class="slider" id="changeVol-${index}" onchange="changeVol(${index})" step="0.05" min="0" max="1" value="1"` +
        `               style="display:none;">` +
        `</div>` +
        `<div class="box column">` +
        `        <select class="dropdown" id="audioSpeed-${index}" onchange="changeSpeed(${index})">` +
        '            <option value="0.5">0.5</option>' +
        '            <option value="1" selected>1</option>' +
        '            <option value="1.25">1.25</option>' +
        '            <option value="1.5">1.5</option>' +
        '            <option value="1.75">1.75</option>' +
        '            <option value="2">2</option>' +
        '        </select>' +
        `</div>` +
        `<div class="column">
            <a href="${compositionList[index].path}" download>
                <img src="../img/download.png" alt="download file" title="Скачать файл">
            </a>
        </div>` +
        `<div class="column">
        <label for="file-upload" class="custom-file-upload">
                <img src="../img/upload.png" alt="upload file" title="Загрузить файл">
        </label>
        <input id="file-upload" type="file"/>
        </div>`;
    document.body.appendChild(audioContainer);
    //set audio duration:
    player = document.getElementById(`audioPlayer-${index}`);
    player.addEventListener('loadedmetadata', () => {
        console.log(`this is duration: ${player.duration}`);
        document.getElementById(`left-time-${index}`).innerHTML = convertEllapsedTime(player.duration);
    }, false);
};
handleAudio = index => {
    player = document.getElementById(`audioPlayer-${index}`);
    playBtn = document.getElementById(`playBtn-${index}`);

    if (playBtn.classList.contains("pause")) {
        //check that all other players are not playing, if any is playing, shut it down and change icons
        let players = document.getElementsByClassName(`playerContainer`);
        console.log(`this is number of players on form: ${players.length}`);
        /*      for (let pl of players) {
                  // set all to pause
                  pl.getElementsByTagName(`audio`)[0].pause();
                  pl.getElementsByClassName(`audioBtn pause img`)[0].src = '../img/play-button.png';
              }*/
        player.play();
        playBtn.classList.add("play");
        playBtn.classList.remove("pause");
        playBtn.src = '../img/pause-button.png';
    } else {
        player.pause();
        playBtn.classList.add("pause");
        playBtn.classList.remove("play");
        playBtn.src = '../img/play-button.png';
    }
    player.addEventListener('timeupdate', function () {
        //change timing:
        let currentTime = player.currentTime;
        let duration = player.duration;

        document.getElementById(`current-time-${index}`).innerHTML = convertEllapsedTime(currentTime);
        document.getElementById(`left-time-${index}`).innerHTML = convertEllapsedTime(duration - currentTime);

        myBar = document.getElementById(`myBar-${index}`);
        progressBar = document.getElementById(`myProgress-${index}`);
        myBar.style.width = currentTime / duration * 100 + '%';
        console.log(`this is current width: ${myBar.style.width}`);

        progressBar.addEventListener("click", seek);
        player = document.getElementById(`audioPlayer-${index}`
        );

        function seek(e) {
            let percent = e.offsetX / this.offsetWidth;
            console.log(
                `this is progress in %: ${percent}`
            );
            player.currentTime = percent * player.duration;
            myBar.style.width = currentTime / duration * 100 + '%';
        }
    });
};
changeVol = (index) => {
    player = document.getElementById(
        `audioPlayer-${index}`
    );
    volume = document.getElementById(
        `changeVol-${index}`
    );
    player.volume = volume.value;
    volume.style.display = 'none';
};
showVolumeRange = (index) => {
    volume = document.getElementById(
        `changeVol-${index}`
    );
    if (window.getComputedStyle(volume).display === "none") {
        volume.style.display = '';
    } else {
        volume.style.display = 'none';
    }
};
changeSpeed = index => {
    speed = document.getElementById(
        `audioSpeed-${index}`
    );
    player = document.getElementById(
        `audioPlayer-${index}`
    );
    player.playbackRate = speed.options[speed.selectedIndex].value;
};
convertEllapsedTime = secondsParam => {
    if (secondsParam) {
        let seconds = Math.floor(secondsParam % 60);
        let minutes = Math.floor(secondsParam / 60);
        return (seconds < 10) ? (minutes + ":0" + seconds) : (minutes + ":" + seconds);
    } else {
        return "0:00";
    }
};