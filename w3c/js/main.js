let player, playBtn, volume, speed, audioDuration, canvas;
let trackList = [{
    src: "../audios/hiti_2019_-_tima_belorusskih_-_vitaminka.mp3",
    title: "Тима Белорусских. Витаминка"
},];
let canvasWidth = 300;

document.addEventListener("DOMContentLoaded", function () {
    startplayer();
}, false);

function startplayer() {
    player = document.getElementById("audio");
    playBtn = document.getElementById("playBtn");
    volume = document.getElementById("change_vol");
    speed = document.getElementById("audio_speed");
    canvas = document.getElementById("progress").getContext('2d');
    canvas.fillRect(0, 0, canvasWidth, 10);

    player.controls = false;
    player.addEventListener('loadedmetadata', function () {
        audioDuration = player.duration;
        let currentTime = player.currentTime;
        if (currentTime) {
            document.getElementById("duration").innerHTML = convertEllapsedTime(audioDuration);
        }
        document.getElementById("current-time").innerHTML = convertEllapsedTime(currentTime);
        //canvas.fillRect(0, 0, canvasWidth, 10);
        //player.setAttribute('duration', player.duration);
    }, false);
}

function handleAudio() {
    if (playBtn.classList.contains("pause")) {
        player.play();
        playBtn.classList.add("play");
        playBtn.classList.remove("pause");
        playBtn.src = 'img/pause-button.png';
    } else {
        player.pause();
        playBtn.classList.add("pause");
        playBtn.classList.remove("play");
        playBtn.src = 'img/play-button.png';
    }
}

function changeVol() {
    player.volume = volume.value;
    volume.style.display = 'none';
}

function changeSpeed() {
    // get option value and set it:
    player.playbackRate = speed.options[speed.selectedIndex].value;
}

function showVolumeRange() {
    if (window.getComputedStyle(volume).display === "none") {
        volume.style.display = '';
    } else {
        volume.style.display = 'none';

    }
}

function convertEllapsedTime(secondsParam) {
    if (secondsParam) {
        let seconds = Math.floor(secondsParam % 60);
        let minutes = Math.floor(secondsParam / 60);
        return (seconds < 10) ? (minutes + ":0" + seconds) : (minutes + ":" + seconds);
    } else {
        return "0:00";
    }

}

function updateBar() {
    canvas.clearRect(0, 0, canvasWidth, 10);
    canvas.fillStyle = '#000';
    canvas.fillRect(0, 0, canvasWidth, 10);

    let currentTime = player.currentTime;
    let duration = player.duration;
    document.getElementById("current-time").innerHTML = convertEllapsedTime(currentTime);
    document.getElementById("left-time").innerHTML = convertEllapsedTime(duration - currentTime);
    let percentage = currentTime / duration;
    let inProgress = (canvasWidth * percentage);
    canvas.fillStyle = "#FF0000";
    canvas.fillRect(0, 0, inProgress, 50);
}
