import "./scss/main.scss";

console.log('started up');

/*function uclicked() {
    console.log('You clicked');
    let h = document.getElementById('hamburger');
    h.style.display = none;
}*/
/*
document.getElementById('hamburger').onClick = function () {
    console.log(`this is called`);
};*/

window.onload = prepareButtons();


function prepareButtons() {
    let navItems = document.getElementsByClassName('nav-link');
    let hamburger = document.getElementById('hamburger');
    let navbar = document.getElementById('navbar');

    hamburger.onclick = () => {
        navbarHandle(true)
    };
    Array.from(navItems).forEach(item => {
        item.onclick = () => {
            navbarHandle(false);
        }
    });

    function navbarHandle(display) {
        if (display && !navbar.classList.contains('displayed')) {
            navbar.classList.add('displayed');
        } else if (!display && navbar.classList.contains('displayed'))
            navbar.classList.remove('displayed');
    }
}
